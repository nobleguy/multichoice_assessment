
=begin
-------------------------------------------------------------------------------------|
@Author: Tshepo "NobleGuy" Moganedi						     |
@email:tmoganedi@gmail.com							     |
@Date Modified:28 June 2016
										     |
Tile is the singleton class that will represents a node		     |
-------------------------------------------------------------------------------------|
=end

require_relative "tile"

class Flatland < Tile
  self.movement_letter = '.'
  self.movement_cost = 1
end

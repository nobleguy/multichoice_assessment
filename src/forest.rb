=begin
-------------------------------------------------------------------------------------|
@Author: Tshepo "NobleGuy" Moganedi						     |
@email:tmoganedi@gmail.com							     |
@Date Modified:28 June 2016
										     |
Forest  class  represents items  in the flatland		             |
-------------------------------------------------------------------------------------|
=end

require_relative "tile"

class Forest < Tile
  self.movement_letter = '*'
  self.movement_cost   = 2
end
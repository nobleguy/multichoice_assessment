=begin
-------------------------------------------------------------------------------------|
@Author: Tshepo "NobleGuy" Moganedi						     |
@contact:tmoganedi@gmail.com							     |
@Date Modified:28 June 2016
										     |
Map  class  represents posiible path  , its a two-dimensional array of node	     |
-------------------------------------------------------------------------------------|
=end

require_relative "tile"
require_relative "position"
require_relative "flatland"
require_relative "start"
require_relative "goal"
require_relative "forest"
require_relative "mountain"
require_relative "water"


NodeClassByLetter = {}
[Flatland, Start, Goal, Water, Forest, Mountain].each do |klass|
  NodeClassByLetter[klass.movement_letter] = klass
end
class Map
  include Enumerable # for find

  def initialize(io)
    @tiles = []
    y_pos = 0
    io.each_line do |line|
      x_pos = 0
      @tiles[y_pos] = []
      line.chomp.split(//).each do |movement_letter|
        @tiles[y_pos] << NodeClassByLetter[movement_letter].new(Position.new(x_pos, y_pos))
        x_pos += 1
      end
      y_pos += 1
      @width  = x_pos
    end
    @height = y_pos
  end

  # Returns true if the given position is on the map
  def contains?(pos)
    pos.x_pos >= 0 and pos.x_pos < @width and pos.y_pos >= 0 and pos.y_pos < @height
  end

  # Return tile at position
  def at(pos)
    @tiles[pos.y_pos][pos.x_pos]
  end

  # Iterate all tiles
  def each
    @tiles.each do |row|
      row.each do |tile|
        yield(tile)
      end
    end
  end

  # Iterates through all  tiles
  def each_neighbour(tile)
    pos = tile.position
    yield_it = lambda{|p| yield(at(p)) if contains? p}
    yield_it.call(pos.relative(-1, -1))
    yield_it.call(pos.relative( 0, -1))
    yield_it.call(pos.relative( 1, -1))
    yield_it.call(pos.relative(-1,  0))
    yield_it.call(pos.relative( 1,  0))
    yield_it.call(pos.relative(-1,  1))
    yield_it.call(pos.relative( 0,  1))
    yield_it.call(pos.relative( 1,  1))
  end

  def to_s
    @tiles.collect{|row|
      row.collect{|node| node.to_s}.join('')
    }.join("\n")
  end
end

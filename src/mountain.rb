=begin
-------------------------------------------------------------------------------------|
@Author: Tshepo "NobleGuy" Moganedi						     |
@email:tmoganedi@gmail.com							     |
@Date Modified:28 June 2016
										     |
Mountain class  will represents a start node  or entrance in the forest		             |
-------------------------------------------------------------------------------------|
=end

require_relative "tile"

class Mountain < Tile
  self.movement_letter = '^'
  self.movement_cost   = 3
end

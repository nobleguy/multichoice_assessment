=begin
-------------------------------------------------------------------------------------|
@Author: Tshepo "NobleGuy" Moganedi						     |
@email:tmoganedi@gmail.com							     |
@Date Modified:28 June 2016
										     |
Start  will represents a start node  or entrance in the forest		             |
-------------------------------------------------------------------------------------|
=end

require_relative "flatland"

class Start < Flatland
  self.movement_letter = '@'
end
=begin
-------------------------------------------------------------------------------------|
@Author: Tshepo "NobleGuy" Moganedi						     |
@email:tmoganedi@gmail.com							     |
@Date Modified:28 June 2016
										     |
Position class represents a  position on the map		             |
-------------------------------------------------------------------------------------|
=end

# An (x_pos, y_pos) position on the map
class Position
  attr_accessor :x_pos, :y_pos

  def initialize(x_pos, y_pos)
    @x_pos, @y_pos = x_pos, y_pos
  end

  def ==(other)
    return false unless Position===other
    @x_pos == other.x_pos and @y_pos == other.y_pos
  end

  # Manhattan calculations
  def distance(other)
    (@x_pos - other.x_pos).abs + (@y_pos - other.y_pos).abs
  end

  # Get a position relative to this node
  def relative(x_posr, y_posr)
    Position.new(x_pos + x_posr, y_pos + y_posr)
  end
end
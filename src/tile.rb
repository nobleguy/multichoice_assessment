=begin
-------------------------------------------------------------------------------------|
@Author: Tshepo "NobleGuy" Moganedi						     |
@email:tmoganedi@gmail.com							     |
@Date Modified:28 June 2016
										     |
Tile is the singleton class that will represents a tile in the game		     |
-------------------------------------------------------------------------------------|
=end
class Tile
  include Comparable # by total_cost

  class << self
    # each tile class is defined by a "map letter" and a cost (1, 2, 3)
    attr_accessor :movement_letter, :movement_cost
  end

  attr_accessor :position, :parent, :movement_cost, :cost_estimated

  def initialize(position)
    @position = position
    @movement_cost = 0
    @cost_estimated = 0
    @on_path = false
    @parent = nil
  end

  def mark_path
    @on_path = true
    @parent.mark_path if @parent
  end

  def walkable?
    true # except Water
  end

  def total_cost
    movement_cost + cost_estimated
  end

  def <=> other
    total_cost <=> other.total_cost
  end

  def == other
    position == other.position
  end

  def to_s
    @on_path ? '#' : self.class.movement_letter
  end
end
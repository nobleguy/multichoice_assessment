=begin
-------------------------------------------------------------------------------------|
@Author: Tshepo "NobleGuy" Moganedi						     |
@email:tmoganedi@gmail.com							     |
@Date Modified:28 June 2016
										     |
Appliaction  main entry point						             |
-------------------------------------------------------------------------------------|
=end
require_relative "map"
require_relative "pathfinder"
require "nokogiri"

abort "usage: #$0 <mapfile>" unless ARGV.size == 1
map = Map.new(File.open(ARGV[0]))
finder = PathFinder.new
finder.find_path(map)
puts map
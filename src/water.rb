=begin
-------------------------------------------------------------------------------------|
@Author: Tshepo "NobleGuy" Moganedi						     |
@email:tmoganedi@gmail.com							     |
@Date Modified:28 June 2016
										     |
Water  will represents obstacle or no go area in the flatland		             |
-------------------------------------------------------------------------------------|
=end

require_relative "tile"

class Water < Tile
  self.movement_letter = '~'
  def walkable?
    false
  end
end

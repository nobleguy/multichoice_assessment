=begin
-------------------------------------------------------------------------------------|
@Author: Tshepo "NobleGuy" Moganedi						     |
@email:tmoganedi@gmail.com							     |
@Date Modified:28 June 2016
										     |
PathFinder class  represents alternative route search  		     |
-------------------------------------------------------------------------------------|
=end

require_relative "tile"

class PathFinder
  def find_path(map)
    start = map.find{|tile| Start === tile}
    goal  = map.find{|tile| Goal  === tile}
    open_set   = [start] # all tiles that are still need to be examined
    closed_set = []      # tiles  already visited

    loop do
      current = open_set.min # find tile with minimum movement_cost
      raise "There is no path from #{start} to #{goal}" unless current
      map.each_neighbour(current) do |tile|
        if tile == goal # we made it!
          tile.parent = current
          tile.mark_path
          return
        end
        next unless tile.walkable?
        next if closed_set.include? tile
        movement_cost = current.movement_cost + tile.class.movement_cost
        if open_set.include? tile
          if movement_cost < tile.movement_cost # but it's cheaper from current tile!
            tile.parent = current
            tile.movement_cost   = movement_cost
          end
        else # we haven't seen this tile
          open_set << tile
          tile.parent = current
          tile.movement_cost   = movement_cost
          tile.cost_estimated = tile.position.distance(goal.position)
        end
      end
      # move "current" from open to closed set:
      closed_set << open_set.delete(current)
    end
  end
end

=begin
-------------------------------------------------------------------------------------|
@Author: Tshepo "NobleGuy" Moganedi						     |
@email:tmoganedi@gmail.com							     |
@Date Modified:28 June 2016
										     |
Goal class  will represents our target or destination in flatland                    |
-------------------------------------------------------------------------------------|
=end

require_relative "flatland"

class Goal < Flatland
  self.movement_letter = 'X'
end
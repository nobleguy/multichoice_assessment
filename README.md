Multichoice Assesment Test: A* algorithim problem

Author: Tshepo "Nobleguy" Moganedi
email: tmoganedi@gmail.com


Instructions on how to run the application

1. Install following dependency 
   -> gem
   -> gem install Nokogiri
2. Extractions of paths from HTML files:
   -> ruby extract_paths.rb file.html
   -> new files will be copied to "test_data" directory  
3. Run the application
   -> in "bin" directory, run
   -> ./run_app

NB: The instructions are based only for  linux operating system.    

require "nokogiri"

abort "usage: #$0 <mapfile>" unless ARGV.size == 1
time = Time.new

doc = Nokogiri::HTML(open(ARGV[0]))
#puts doc.css('pre').length
#doc.css('pre')[1].text
puts doc.xpath("//pre").text
  
begin
  file = File.open("../test_data/test_map_#{time.strftime("%Y_%m_%d")}.txt", "w")
  file.write(doc.xpath("//pre").text) 
rescue IOError => e
  puts "some error occur, dir not writable etc."
ensure
  file.close unless file.nil?
end

